package interceptors

func AccessibleRoles() map[string][]string {
	const userServicePath = "/johndanielcurley.auth.UserService/"

	return map[string][]string{
		userServicePath + "CreateUser": {"admin", "user"},
		userServicePath + "GetUser":    {"admin", "user"},
		userServicePath + "UpdateUser": {"admin", "user"},
		userServicePath + "DeleteUser": {"admin", "user"},
	}
}
