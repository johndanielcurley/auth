package auth

import (
	"context"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"gitlab.com/johndanielcurley/auth/connection"
	"gitlab.com/johndanielcurley/auth/pb"
	views "gitlab.com/johndanielcurley/auth/views/tokens"
	user "gitlab.com/johndanielcurley/auth/views/user"
)

type AuthServer struct {
	pb.UnimplementedAuthServiceServer
	jwtManager *views.JWTManager
	conn       *connection.Connection
}

func NewAuthServer(conn *connection.Connection, jwtManager *views.JWTManager) *AuthServer {
	return &AuthServer{conn: conn, jwtManager: jwtManager}
}

func (server *AuthServer) Login(ctx context.Context, req *pb.LoginRequest) (*pb.LoginResponse, error) {
	user := user.FindUserByUsername(server.conn, req.GetUsername())
	if user == nil {
		return nil, status.Errorf(codes.NotFound, "cannot find user")
	}


	if !user.IsCorrectPassword(req.GetPassword()) {
		return nil, status.Errorf(codes.NotFound, "incorrect password")
	}

	accessToken, refreshToken, err := server.jwtManager.GenerateTokenPair(ctx, user)
	if err != nil {
		return nil, status.Errorf(codes.Internal, "cannot generate token pair")
	}

	res := &pb.LoginResponse{AccessToken: accessToken, RefreshToken: refreshToken}
	return res, nil
}

/*
func (server *AuthServer) Logout(conn *connection.Connection, ctx context.Context, req *pb.LogoutRequest) error {

	user := user.FindUserByUsername(conn, req.GetUserID())
	conn.Redis.Del(ctx, fmt.Sprintf("token-%d", user.ID))

	return status.Errorf(codes.OK, "user logged out")
}

func (server *AuthServer) Register(ctx context.Context, req *pb.RegisterRequest) (*pb.RegisterResponse, error) {

}
*/

/*
func (server *AuthServer) ResetPassword() error {

}
*/
