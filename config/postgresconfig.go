package config

import "os"

type DBConfig struct {
	Addr     string
	Port     string
	Database string
	User     string
	Password string
	Ssl      string
}

func LoadDBConfig() DBConfig {
	return DBConfig{
		Addr:     os.Getenv("DB_ADDR"),
		Port:     os.Getenv("DB_PORT"),
		Database: os.Getenv("DB_DATABASE"),
		User:     os.Getenv("DB_USER"),
		Password: os.Getenv("DB_PASSWORD"),
		Ssl:      os.Getenv("DB_SSL"),
	}
}