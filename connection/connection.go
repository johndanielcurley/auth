package connection

import (
	"context"

	"github.com/go-redis/redis/v8"
	"github.com/uptrace/bun"
	"gitlab.com/johndanielcurley/auth/config"
)

type Connection struct {
	Redis redis.Client
	DB    bun.DB
	Ctx   context.Context
}

func NewConnection(cfg *config.Config) *Connection {
	ctx := context.Background()
	return &Connection{
		Redis: *InitRedis(cfg),
		DB:    *InitDB(ctx, cfg),
		Ctx:   ctx,
	}
}
