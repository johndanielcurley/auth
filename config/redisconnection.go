package config

import (
	"os"
	"strconv"
)

type RedisConfig struct {
	Address  string
	Port     string
	Password string
	DB       int
}

func LoadRedisConfig() RedisConfig {
	dbNumber, err := strconv.Atoi(os.Getenv("REDIS_DB"))
	if err != nil {
		panic(err)
	}
	redisConf := RedisConfig{
		Address:  os.Getenv("REDIS_ADDRESS"),
		Port:     os.Getenv("REDIS_PORT"),
		Password: os.Getenv("REDIS_PASSWORD"),
		DB:       dbNumber,
	}

	return redisConf

}
