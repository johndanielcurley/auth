package config

import (
	"crypto/ed25519"
	"encoding/asn1"
	"encoding/pem"
	"io/ioutil"
	"os"
	"time"
)

type ed25519PrivKey struct {
	Version          int
	ObjectIdentifier struct {
		ObjectIdentifier asn1.ObjectIdentifier
	}
	PrivateKey []byte
}

type ed25519PubKey struct {
	ObjectIdentifier struct {
		ObjectIdentifier asn1.ObjectIdentifier
	}
	PublicKey asn1.BitString
}

type AuthConfig struct {
	PublicKey            ed25519.PublicKey
	PrivateKey           ed25519.PrivateKey
	AccessTokenDuration  time.Duration
	RefreshTokenDuration time.Duration
}

func LoadAuthConfig() AuthConfig {
	var block *pem.Block
	asn1PublicKey := ed25519PubKey{}
	asn1PrivateKey := ed25519PrivKey{}

	privKeyPEM, err := ioutil.ReadFile(os.Getenv("PRIV_KEY_FILE"))
	if err != nil {
		err.Error()
	}
	block, _ = pem.Decode(privKeyPEM)
	asn1.Unmarshal(block.Bytes, &asn1PrivateKey)
	privateKey := ed25519.NewKeyFromSeed(asn1PrivateKey.PrivateKey[2:])

	pubKeyPEM, err := ioutil.ReadFile(os.Getenv("PUB_KEY_FILE"))
	if err != nil {
		err.Error()
	}
	block, _ = pem.Decode(pubKeyPEM)
	asn1.Unmarshal(block.Bytes, &asn1PublicKey)
	publicKey := ed25519.PublicKey(asn1PublicKey.PublicKey.Bytes)

	return AuthConfig{
		PublicKey:  publicKey,
		PrivateKey: privateKey,
	}
}
