package connection

import (
	"fmt"

	redis "github.com/go-redis/redis/v8"
	"gitlab.com/johndanielcurley/auth/config"
)

func InitRedis(cfg *config.Config) *redis.Client {
	addr := fmt.Sprintf("%s:%s", cfg.Redis.Address, cfg.Redis.Port)

	return redis.NewClient(&redis.Options{
		Addr:     addr,
		Password: cfg.Redis.Password,
		DB:       cfg.Redis.DB,
	})
}
