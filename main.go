package main

import (
	"gitlab.com/johndanielcurley/auth/config"
	"gitlab.com/johndanielcurley/auth/connection"
	"gitlab.com/johndanielcurley/auth/server"
)

func main() {
	cfg := config.NewConfig()
	conn := connection.NewConnection(cfg)
	server.NewServer(cfg, conn)
}
