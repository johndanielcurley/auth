package connection

import (
	"context"
	"database/sql"
	"os"

	"github.com/uptrace/bun"
	"github.com/uptrace/bun/dbfixture"
	"github.com/uptrace/bun/dialect/pgdialect"
	"github.com/uptrace/bun/driver/pgdriver"

	"gitlab.com/johndanielcurley/auth/config"
	"gitlab.com/johndanielcurley/auth/models"
)

func InitDB(ctx context.Context, cfg *config.Config) *bun.DB {
	/*
		pgconn := pgdriver.NewConnector(
			pgdriver.WithAddr(cfg.DB.Addr+":"+cfg.DB.Port),
			pgdriver.WithTLSConfig(&tls.Config{InsecureSkipVerify: true}),
			pgdriver.WithUser(cfg.DB.User),
			pgdriver.WithPassword(cfg.DB.Password),
			pgdriver.WithDatabase(cfg.DB.Database),
		)
	*/

	dsn := "postgresql://auth:@localhost:5432/auth?sslmode=disable"
	b := sql.OpenDB(pgdriver.NewConnector(pgdriver.WithDSN(dsn)))

	db := bun.NewDB(b, pgdialect.New())


	db.RegisterModel((*models.User)(nil))

	fixture := dbfixture.New(db, dbfixture.WithRecreateTables())
	if err := fixture.Load(ctx, os.DirFS("."), "fixture.yml"); err != nil {
		panic(err)
	}

	return db
}
