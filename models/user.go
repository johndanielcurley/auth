package models

import (
	"fmt"

	"github.com/alexedwards/argon2id"
	"github.com/badoux/checkmail"
	"github.com/google/uuid"
)

type User struct {
	Username       string
	ID             uuid.UUID 
	Email          string
	HashedPassword string
	Role           string
}

func NewUser(username string, email string, password string, role string) (*User, error) {
	hashedPassword, err := argon2id.CreateHash(password, argon2id.DefaultParams)
	if err != nil {
		return nil, fmt.Errorf("cannot hash password: %w", err)
	}

	user := &User{
		Username:       username,
		ID:             uuid.New(),
		Email:          email,
		HashedPassword: hashedPassword,
		Role:           role,
	}

	return user, nil
}

func (user *User) IsCorrectPassword(password string) bool {
	_, err := argon2id.ComparePasswordAndHash(password, user.HashedPassword)
	return err == nil
}

func (user *User) IsValidEmail(email string) bool {
	err := checkmail.ValidateFormat(email)
	if err != nil {
		return false
	}
	err = checkmail.ValidateHost(email)
	return err == nil
}
