package server

import (
	"log"
	"net"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"

	"gitlab.com/johndanielcurley/auth/config"
	"gitlab.com/johndanielcurley/auth/connection"
	"gitlab.com/johndanielcurley/auth/interceptors"
	"gitlab.com/johndanielcurley/auth/pb"
	"gitlab.com/johndanielcurley/auth/views/tokens"
	"gitlab.com/johndanielcurley/auth/views/auth"
)

func NewServer(cfg *config.Config, conn *connection.Connection) {
	jwtManager := tokens.NewJWTManager(cfg, conn)
	authServer := auth.NewAuthServer(conn, jwtManager)

	interceptor := interceptors.NewAuthInterceptor(jwtManager, interceptors.AccessibleRoles())
	grpcServer := grpc.NewServer(
		grpc.UnaryInterceptor(interceptor.Unary()),
		grpc.StreamInterceptor(interceptor.Stream()),
	)
	pb.RegisterAuthServiceServer(grpcServer, authServer)
	reflection.Register(grpcServer)
	// TODO implement tls from certificate for encryption security
	addr := cfg.Http.Host + ":" + cfg.Http.Port
	log.Printf("Start GRPC server on address: %s", addr)
	listener, err := net.Listen("tcp", addr)
	if err != nil {
		log.Fatal("cannot start server: ", err)
	}

	err = grpcServer.Serve(listener)
	if err != nil {
		log.Fatal("cannot start server: ", err)
	}
}
