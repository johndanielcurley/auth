package user 

import (
	"log"

	"github.com/google/uuid"
	"github.com/uptrace/bun/driver/pgdriver"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"gitlab.com/johndanielcurley/auth/connection"
	"gitlab.com/johndanielcurley/auth/models"
)

func CreateUser(conn *connection.Connection, userName string, email string, password string, role string) (*models.User, error) {
	user, _ := models.NewUser(userName, email, password, role)

	exists := FindUserByUsername(conn, userName)
	if exists.Username != "" {
		return nil, status.Errorf(codes.AlreadyExists, "user exists")
	}

	if user.IsValidEmail(email) {
		return nil, status.Errorf(codes.InvalidArgument, "not a valid email address")
	}

	// add the user to the database
	_, err := conn.DB.NewInsert().Model(user).Exec(conn.Ctx)
	if err != nil {
		log.Println(err.(pgdriver.Error))
	}

	return user, nil
}

func FindUserByUsername(conn *connection.Connection, userName string) *models.User {
	user := new(models.User)

	err := conn.DB.NewSelect().Model(user).Where("username = ?", userName).Scan(conn.Ctx)
	if err != nil {
		log.Println(err.(pgdriver.Error))
	}

	if user.Username == "" {
		return nil
	}

	return user
}

func FindUserByID(conn *connection.Connection, id uuid.UUID) *models.User {
	user := new(models.User)

	err := conn.DB.NewSelect().Model(user).Where("id = ?", id).Scan(conn.Ctx)
	if err != nil {
		log.Println(err.(pgdriver.Error))
	}

	if user.Username == "" {
		return nil
	}

	return user
}

func FindUserByEmailAddress(conn *connection.Connection, email string) *models.User {
	user := new(models.User)

	err := conn.DB.NewSelect().Model(user).Where("email = ?", email).Scan(conn.Ctx)
	if err != nil {
		log.Println(err.(pgdriver.Error))
	}

	if user.Username == "" {
		return nil
	}

	return user
}

/*
func UpdateUser(conn *connection.Connection, id uuid.UUID) {
	user := new(models.User)


}



func ResetUserPassword(id uuid.UUID) {
	// TODO
}
*/
