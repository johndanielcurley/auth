package tokens

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/status"

	"gitlab.com/johndanielcurley/auth/config"
	"gitlab.com/johndanielcurley/auth/connection"

	uuid "github.com/google/uuid"
	"github.com/pascaldekloe/jwt"

	"gitlab.com/johndanielcurley/auth/models"
)

type JWTManager struct {
	privateKey           []byte
	publicKey            []byte
	RefreshtokenDuration time.Duration
	AccessTokenDuration  time.Duration
	connection           *connection.Connection
}

type CachedTokens struct {
	AuthorizeUID string `json:"access"`
	RefreshUID   string `json:"refresh"`
}

func NewJWTManager(cfg *config.Config, conn *connection.Connection) *JWTManager {
	return &JWTManager{cfg.Auth.PrivateKey,
		cfg.Auth.PublicKey,
		cfg.Auth.AccessTokenDuration,
		cfg.Auth.RefreshTokenDuration,
		conn}
}

func (manager *JWTManager) Generate(claims jwt.Claims) (string, error) {
	token, err := claims.EdDSASign(manager.privateKey)
	return string(token), err
}

func (manager *JWTManager) GenerateTokenPair(ctx context.Context, user *models.User) (
	accessToken string,
	refreshToken string,
	err error,
) {
	var accessClaims jwt.Claims
	accessUID := uuid.New().String()
	accessClaims.Expires = jwt.NewNumericTime(time.Now().Add(time.Minute * manager.AccessTokenDuration))
	accessClaims.Set = map[string]interface{}{"ID": user.ID, "UID": accessUID, "Role": user.Role}
	accessToken, err = manager.Generate(accessClaims)
	if err != nil {
		fmt.Println("access token creation failed on", err)
	}

	var refreshClaims jwt.Claims
	refreshUID := uuid.New().String()
	refreshClaims.Expires = jwt.NewNumericTime(time.Now().Add(time.Minute * manager.RefreshtokenDuration))
	refreshClaims.Set = map[string]interface{}{"ID": user.ID, "UID": refreshUID}
	refreshToken, err = manager.Generate(refreshClaims)
	if err != nil {
		fmt.Println("refresh token creation failed on", err)
	}

	cacheJSON, err := json.Marshal(CachedTokens{
		AuthorizeUID: accessUID,
		RefreshUID:   refreshUID,
	})

	manager.connection.Redis.Set(ctx, fmt.Sprintf("token-%d", user.ID), string(cacheJSON), 0)

	return accessToken, refreshToken, nil
}

func (manager *JWTManager) Verify(ctx context.Context, claims *jwt.Claims, isRefresh bool) error {
	var cacheJSON, _ = manager.connection.Redis.Get(ctx, fmt.Sprintf("token-%s", claims.ID)).Result()
	cachedTokens := new(CachedTokens)
	err := json.Unmarshal([]byte(cacheJSON), cachedTokens)

	var tokenUID string
	if isRefresh {
		tokenUID = cachedTokens.RefreshUID
	} else {
		tokenUID = cachedTokens.AuthorizeUID
	}

	if err != nil || tokenUID != claims.Set["UID"] {
		return status.Errorf(codes.Unauthenticated, "token not found")
	}

	if !claims.Valid(time.Now()) {
		return status.Errorf(codes.Unauthenticated, "credential time constraints exceeded")
	}

	return nil
}

func (manager *JWTManager) ParseToken(token string) (*jwt.Claims, error) {
	claims, err := jwt.EdDSACheck([]byte(token), manager.publicKey)
	if err != nil {
		return nil, status.Errorf(codes.Unauthenticated, "unexpected token signing method")
	}

	return claims, nil
}

func (manager *JWTManager) RefreshToken(ctx context.Context) error {
	md, ok := metadata.FromIncomingContext(ctx)
	if !ok {
		return status.Errorf(codes.Unauthenticated, "metadata is not provided")
	}

	values := md["refresh"]
	if len(values) == 0 {
		return status.Errorf(codes.Unauthenticated, "refresh token is not provided")
	}

	refreshToken := values[0]
	claims, err := manager.ParseToken(refreshToken)
	if err != nil {
		return err
	}

	err = manager.Verify(ctx, claims, true)
	if err != nil {
		return err
	}

	return nil
}
