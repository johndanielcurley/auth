package config

import (
	"log"

	"github.com/joho/godotenv"
)

type Config struct {
	Auth AuthConfig
	Http HttpConfig
	Redis RedisConfig
	DB 		DBConfig
}

func NewConfig() *Config {
	err := godotenv.Load()
	if err != nil {
		log.Println("Error loading .env file")
	}

	return &Config{
		Auth: LoadAuthConfig(),
		Http: LoadHTTPConfig(),
		DB: LoadDBConfig(),
		Redis: LoadRedisConfig(),
	}
}